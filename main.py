from aiogram import Bot, Dispatcher, executor, types
from handlers import register_all_handlers
from tokens import TG_TOKEN


async def __on_start_up(dp: Dispatcher) -> None:
    register_all_handlers(dp)


def start_bot() -> None:
    bot = Bot(token=TG_TOKEN, parse_mode=types.ParseMode.HTML)
    dp = Dispatcher(bot=bot)
    executor.start_polling(dp, skip_updates=True, on_startup=__on_start_up)


if __name__ == '__main__':
    start_bot()
