from aiogram import Dispatcher, types
from .user_handlers import user_handlers


def register_all_handlers(dp: Dispatcher) -> None:
    for handler in user_handlers:
        dp.register_message_handler(handler['func'],
                                    commands=None if 'commands' not in handler else handler['commands'],
                                    content_types=types.ContentType.ANY if 'content_types' not in handler
                                    else handler['content_types'])
