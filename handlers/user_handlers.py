from aiogram import types
from misc import log
from urllib.error import HTTPError
import aiohttp
from tokens import KP_TOKEN
from random import randint

TYPE_RU = {
    'movie': 'Фильм',
    'tv-series': 'Сериал',
    'cartoon': 'Мультфильм',
    'anime': 'Аниме',
    'animated-series': 'Мультсериал',
    'tv-show': 'ТВ-шоу'
}

HEADERS = {
    'accept': 'application/json',
    'X-API-KEY': KP_TOKEN
}


async def cmd_start(message: types.Message) -> None:
    answer = await message.answer_sticker('CAACAgIAAxkBAAIF4mR8LsHVopkbKI-B7w2HuUBMa_A7AALYDwACSPJgSxX7xNp4dGuYLwQ')
    answer1 = await message.answer(f'Здравствуй, {message.from_user.first_name}. Я бот, который поможет тебе с поиском '
                                   f'фильмов и получением информации о них. Используй /help для получения списка '
                                   f'доступных команд. Надеюсь, ты найдёшь то, за чем сюда пришёл!')
    log(message.from_user.id, message, [answer, answer1], message.date)


async def cmd_help(message: types.Message) -> None:
    answer = await message.answer(f'/start - команда для вывода приветственного сообщения новым пользователям. '
                                  f'Никто не запрещает вызвать данную команду ещё раз :)\n'
                                  f'/search <code>название</code> - команда для поиска ленты по названию. '
                                  f'Выдаётся множество найденных вариантов, среди которых пользователь может выбрать '
                                  f'один.\n/inspect <code>ID</code> - команда для получения подробной информации о '
                                  f'картине по её ID, прилагается ссылка на Кинопоиск.\n/similar <code>ID</code> - '
                                  f'команда для получения похожих фильмов и сериалов для конкретной ленты.\n'
                                  f'/recommend <code>жанр количество_рекомендаций</code> - команда для получения '
                                  f'рекомендаций по конкретному жанру, пользователь также может указать количество '
                                  f'получаемых рекомендаций.')
    log(message.from_user.id, message, [answer], message.date)


async def cmd_search(message: types.Message) -> None:
    if not message.get_args():
        answer = await message.answer('Напишите название фильма!')
        log(message.from_user.id, message, [answer], message.date)
        return

    request = 'https://api.kinopoisk.dev/v1.3/movie'
    params = {
        'page': 1,
        'limit': 10,
        'sortField': 'votes.kp',
        'selectFields': ['type', 'name', 'year', 'countries', 'id'],
        'name': message.get_args().split()[0].lower()
    }

    try:
        async with aiohttp.ClientSession(headers=HEADERS) as session:
            async with session.get(request, params=params) as resp:
                answer = await resp.json()

                if answer['total'] == 0:
                    answer = await message.answer('По вашему запросу ничего не найдено!')
                    log(message.from_user.id, message, [answer], message.date)
                    return

                search_number = answer['total'] if answer['total'] <= answer['limit'] else answer['limit']
                answers = [f'Найдено {search_number} результатов:\n']

                for item in answer['docs']:
                    countries = [country['name'] for country in item['countries']]
                    command = f'Больше информации: <code>/inspect {item["id"]}</code>'
                    info = f'{item["name"]} --- {TYPE_RU[item["type"]]} --- {item["year"]} год --- ' \
                           f'{", ".join(countries)}\n{command}\n'
                    answers.append(info)

                answers.append('Используйте команду <code>/inspect ID</code> для получения подробной информации '
                               'о фильме или сериале')

                msg = '\n'.join(answers)
                bot_answer = await message.answer(msg)
                log(message.from_user.id, message, [bot_answer], message.date)
    except HTTPError as err:
        answer = await message.answer(f'Возникла ошибка HTTP: {err}!')
        log(message.from_user.id, message, [answer], message.date)


async def cmd_inspect(message: types.Message) -> None:
    if not message.get_args():
        answer = await message.answer('Напишите ID фильма!')
        log(message.from_user.id, message, [answer], message.date)
        return

    args = message.get_args().split()

    try:
        kp_id = int(args[0])
    except ValueError:
        answer = await message.answer('Укажите ID правильно!')
        log(message.from_user.id, message, [answer], message.date)
        return

    request = f'https://api.kinopoisk.dev/v1.3/movie/{kp_id}'

    try:
        async with aiohttp.ClientSession(headers=HEADERS) as session:
            async with session.get(request) as resp:
                answer = await resp.json()

                if 'statusCode' in answer:
                    answer = await message.answer('По вашему запросу ничего не найдено!')
                    log(message.from_user.id, message, [answer], message.date)
                    return

                movie_length = f'Продолжительность: {answer["movieLength"]} минут\n' if answer["movieLength"] else '\n'
                if answer['description']:
                    desc = answer['description'].replace('\n', '')
                else:
                    desc = 'отсутствует'
                kp_type = 'series' if answer['isSeries'] else 'film'
                kp_link = f'https://www.kinopoisk.ru/{kp_type}/{answer["id"]}/'
                genres = [genre['name'] for genre in answer['genres']]
                countries = [country['name'] for country in answer['countries']]
                rating = answer['rating']['kp'] if answer['rating']['kp'] != 0 else 'недостаточно оценок'
                command = f'Похожие фильмы: <code>/similar {answer["id"]}</code>'

                caption = f'Название: {answer["name"]}\nОписание: {desc}\n\n' \
                          f'Год выпуска: {answer["year"]}\nЖанр: {", ".join(genres)}\n' \
                          f'Страна: {", ".join(countries)}\n' \
                          f'Оценка на Кинопоиске: {rating}\n' + movie_length + f'\n{command}'

                inline = types.InlineKeyboardMarkup(row_width=1)
                inline.add(types.InlineKeyboardButton(text='Больше информации', url=kp_link))

                if answer['poster']:
                    bot_answer = await message.answer_photo(photo=answer['poster']['url'], caption=caption,
                                                            reply_markup=inline)
                else:
                    bot_answer = await message.answer(caption, reply_markup=inline)
                log(message.from_user.id, message, [bot_answer], message.date)
    except HTTPError as err:
        answer = await message.answer(f'Возникла ошибка HTTP: {err}!')
        log(message.from_user.id, message, [answer], message.date)


async def cmd_recommend(message: types.Message) -> None:
    if not message.get_args():
        answer = await message.answer('Для рекомендации необходимо указать жанр и/или количество!')
        log(message.from_user.id, message, [answer], message.date)
        return

    args = message.get_args().split(' ')
    genre = args[0].lower()

    try:
        recommend_count = int(args[1]) if len(args) >= 2 else 5
    except ValueError:
        answer = await message.answer('Укажите количество правильно!')
        log(message.from_user.id, message, [answer], message.date)
        return

    if recommend_count > 5 or recommend_count < 1:
        answer = await message.answer('Неправильное значение для количества!')
        log(message.from_user.id, message, [answer], message.date)
        return

    request = 'https://api.kinopoisk.dev/v1.3/movie'
    params = {
        'page': 1,
        'limit': 50,
        'sortField': 'rating.kp',
        'sortType': -1,
        'genres.name': genre
    }

    try:
        async with aiohttp.ClientSession(headers=HEADERS) as session:
            async with session.get(request, params=params) as resp:
                answer = await resp.json()

                if answer['total'] == 0:
                    answer = await message.answer('По вашему запросу ничего не найдено!')
                    log(message.from_user.id, message, [answer], message.date)
                    return

                answers = []
                for i in range(0, recommend_count):
                    ind = randint(0, len(answer['docs']))
                    item = answer['docs'].pop(ind)
                    kp_type = 'series' if 'releaseYears' in item else 'film'
                    link = f'https://www.kinopoisk.ru/{kp_type}/{item["id"]}'
                    desc = item['description'].replace('\n', '') if 'description' in item else 'отсутствует'

                    info = f'Название: {item["name"]}\nОписание: {desc}\n' \
                           f'Рейтинг на Кинопоиске: {item["rating"]["kp"]}\nГод: {item["year"]}\n' \
                           f'Больше информации: {link}\n'
                    answers.append(info)

                to_answer = '\n'.join(answers)
                answer = await message.answer(f'{recommend_count} рекомендаций:\n{to_answer}')
                log(message.from_user.id, message, [answer], message.date)
    except HTTPError as err:
        await message.answer(f'Возникла ошибка HTTP: {err}!')


async def cmd_similar(message: types.Message) -> None:
    if not message.get_args():
        answer = await message.answer('Напишите ID фильма!')
        log(message.from_user.id, message, [answer], message.date)
        return

    args = message.get_args().split()

    try:
        kp_id = int(args[0])
    except ValueError:
        answer = await message.answer('Укажите ID правильно!')
        log(message.from_user.id, message, [answer], message.date)
        return

    request = f'https://api.kinopoisk.dev/v1.3/movie/{kp_id}'

    try:
        async with aiohttp.ClientSession(headers=HEADERS) as session:
            async with session.get(request) as resp:
                answer = await resp.json()

                if 'statusCode' in answer:
                    answer = await message.answer('По вашему запросу ничего не найдено!')
                    log(message.from_user.id, message, [answer], message.date)
                    return

                if not len(answer['similarMovies']):
                    answer = await message.answer('Похожих фильмов не найдено!')
                    log(message.from_user.id, message, [answer], message.date)
                    return

                answers = [f'Найдено {len(answer["similarMovies"])} результатов:\n']

                for item in answer['similarMovies']:
                    command = f'Больше информации: <code>/inspect {item["id"]}</code>'
                    info = f'{item["name"]} --- {TYPE_RU[item["type"]]}\n{command}\n'
                    answers.append(info)

                answers.append('\nИспользуйте команду <code>/inspect ID</code> для получения подробной информации '
                               'о фильме или сериале')

                msg = '\n'.join(answers)
                bot_answer = await message.answer(msg)
                log(message.from_user.id, message, [bot_answer], message.date)
    except HTTPError as err:
        answer = await message.answer(f'Возникла ошибка HTTP: {err}!')
        log(message.from_user.id, message, [answer], message.date)


async def other_messages(message: types.Message) -> None:
    answer = await message.answer('Я вас не понял. Используйте /help для получения списка доступных команд.')
    log(message.from_user.id, message, [answer], message.date)


user_handlers = [
    {'func': cmd_start, 'commands': {'start'}},
    {'func': cmd_search, 'commands': {'search'}},
    {'func': cmd_inspect, 'commands': {'inspect'}},
    {'func': cmd_recommend, 'commands': {'recommend'}},
    {'func': cmd_similar, 'commands': {'similar'}},
    {'func': cmd_help, 'commands': {'help'}},
    {'func': other_messages}
]
