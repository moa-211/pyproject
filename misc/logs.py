from datetime import datetime
from aiogram import types


def log(user_id: int, message: types.Message, answers: list, date: datetime) -> None:
    with open(f'./logs/{user_id}.log', 'a+', encoding='utf-8') as file:
        if message.reply_to_message:
            reply = f'Ответ на сообщение с ID "{message.reply_to_message.message_id}"'
        elif message.forward_from:
            reply = f'Переслано от {message.forward_from.first_name}'
        else:
            reply = ''

        if message.photo and message.caption:
            from_user = f'Фото с подписью "{message.caption}"'
        elif message.photo:
            from_user = f'Фото без подписи'
        elif message.sticker:
            from_user = f'Стикер с ID "{message.sticker.file_unique_id}"'
        elif message.animation:
            from_user = f'Анимация с ID "{message.animation.thumb.file_unique_id}"'
        elif message.video:
            from_user = f'Видео с ID "{message.video.thumb.file_unique_id}"'
        elif message.document:
            from_user = f'Документ "{message.document.file_name}" с ID "{message.document.file_unique_id}"'
        else:
            from_user = message.text

        user_writing = f'{date} Пользователь: {from_user} ({reply})' if reply else f'{date} Пользователь: {from_user}'
        file.write(user_writing + '\n')

        for answer in answers:
            if answer.photo and answer.caption:
                to_answer = f'Фото с подписью "{answer.caption}"'
            elif answer.sticker:
                to_answer = f'Стикер c ID "{answer.sticker.file_unique_id}"'
            else:
                to_answer = answer.text

            file.write(f'{datetime.now().replace(microsecond=0)} Бот: {to_answer}\n')
